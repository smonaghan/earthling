package earthling.datagen;

public interface Generator<T> {

    T generate();
}
