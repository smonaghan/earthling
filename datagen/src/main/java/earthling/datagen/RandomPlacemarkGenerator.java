package earthling.datagen;


import de.micromata.opengis.kml.v_2_2_0.Placemark;
import earthling.datagen.util.RandomUtils;

public class RandomPlacemarkGenerator implements Generator<Placemark> {

    @Override
    public Placemark generate() {
        Placemark placemark = new Placemark()
                .withName("Earthling datagen @ " + System.currentTimeMillis())
                .withDescription("funzies");

        // nyc 40.664167, -73.938611
        double lat = 40.664167 + RandomUtils.randRange(0.000000,0.000008);
        double lon = -73.938611 + RandomUtils.randRange(0.000000,0.000008);
        placemark.createAndSetLookAt()
                .withLatitude(lat)
                .withLongitude(lon)
                .withRange(440.8)
                .withTilt(33);

        placemark.createAndSetPoint().addToCoordinates(lon + "," + lat);
        return placemark;
    }
}
