package earthling.datagen.util;

public class RandomUtils {

    public static double randRange(double min, double max) {
        return (Math.random() * (max - min + 1)) + min;
    }

    public static double randLat(){
        return randRange(-180, 180);
    }

    public static double randLon(){
        return randRange(-90, 90);
    }

}
