package earthling.datagen;

import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.KmlFactory;
import earthling.datagen.util.RandomUtils;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

public class RandomPlacemarkGeneratorTest {

    @Test
    public void testGenerate() throws FileNotFoundException {
        RandomPlacemarkGenerator generator = new RandomPlacemarkGenerator();
        Kml kml = KmlFactory.createKml();
        kml.setFeature(generator.generate());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        kml.marshal(baos);
        System.out.println(baos.toString());
    }
}
