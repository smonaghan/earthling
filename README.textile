h1. Earthling

Earthling is an exploration of the Google Earth API, KML and GIS in general

h2. Running the demo

> cd earthling
> mvn clean install
> cd web
> mvn jetty:run

"http://localhost:8080":http://localhost:8080

Watch the browser console for info. Occasional javascript 404s due to js dev settings can be addressed by browser refresh

h2. References
* "Google Earth API Reference":https://developers.google.com/earth/documentation/reference/
* "KML Reference":https://developers.google.com/kml/documentation/kmlreference
* "KML Developer's Guide":https://developers.google.com/kml/documentation/topicsinkml
* "Google Earth API Samples":https://code.google.com/p/earth-api-samples/
* "Google Earth Plugin/API release notes":https://developers.google.com/earth/documentation/releasenotes
* "KML Interactive Sampler":http://kml-samples.googlecode.com/svn/trunk/interactive/index.html
* "Modeling a city":http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en/us/intl/en/sketchup/3dwh/pdfs/modeling_a_city.pdf
* "LiDAR Related":https://github.com/seanp33/earthling/wiki/LiDAR-Related
* "Unity Related":https://github.com/seanp33/earthling/wiki/Unity-Related
