package earthling.cometd.domain;

import java.util.List;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;


public class AreaOfInterest {

	private final List<Coordinate> _coordinates;

	public AreaOfInterest(List<Coordinate> coordinates) {
		this._coordinates = coordinates;
	}
	
	public List<Coordinate> getCoordinates() {
		return _coordinates;
	}
	
	
}
