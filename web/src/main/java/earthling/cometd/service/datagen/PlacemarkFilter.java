package earthling.cometd.service.datagen;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Point;
import earthling.cometd.domain.AreaOfInterest;


public class PlacemarkFilter {
	
	
    protected final static Logger logger = LoggerFactory.getLogger(PlacemarkFilter.class);
	

	public static boolean placemarkIsWithinAreaOfInterest(AreaOfInterest aoi, Placemark placemark) {
		if(aoi == null) {
			return false;
		}
		List<Coordinate> coordinates = aoi.getCoordinates();
		int coordinatesSize = coordinates.size();
		int coordinatesLength = coordinates.size();
		Point placemarkPoint = (Point)placemark.getGeometry();
		double placemarkLat = placemarkPoint.getCoordinates().get(0).getLatitude();
		double placemarkLong = placemarkPoint.getCoordinates().get(0).getLongitude();
		Double coordinatesLong[] = new Double[coordinatesLength];
		Double coordinatesLat[] = new Double[coordinatesLength];
		
		for(int x=0; x<coordinatesLength; x++) {
			coordinatesLong[x] = coordinates.get(x).getLongitude();
			coordinatesLat[x] = coordinates.get(x).getLatitude();
		}
		
		
		boolean result = false;
		int x, y = 0;
		for (x = 0, y = coordinatesSize - 1; x < coordinatesSize; y = x++) {

			if (((coordinatesLong[x] > placemarkLong) != (coordinatesLong[y] > placemarkLong))
					&& (placemarkLat < (coordinatesLat[y] - coordinatesLat[x]) * (placemarkLong - coordinatesLong[x])
							/ (coordinatesLong[y] - coordinatesLong[x]) + coordinatesLat[x]))
				result = true;
		}
		return result;
		
	}
}
