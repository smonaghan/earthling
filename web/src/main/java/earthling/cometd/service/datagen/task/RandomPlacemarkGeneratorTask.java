package earthling.cometd.service.datagen.task;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.TimerTask;

import org.cometd.bayeux.server.ServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.KmlFactory;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import earthling.cometd.domain.AreaOfInterest;
import earthling.cometd.service.datagen.DataGenService;
import earthling.cometd.service.datagen.PlacemarkFilter;
import earthling.datagen.RandomPlacemarkGenerator;

public class RandomPlacemarkGeneratorTask extends TimerTask {
    protected final static Logger logger = LoggerFactory.getLogger(RandomPlacemarkGeneratorTask.class);

    private ServerSession serverSession;
    private ServerSession remote;
    private RandomPlacemarkGenerator randomPlacemarkGenerator;

    public RandomPlacemarkGeneratorTask(ServerSession serverSession, ServerSession remote, RandomPlacemarkGenerator randomPlacemarkGenerator) {
        this.serverSession = serverSession;
        this.remote = remote;
        this.randomPlacemarkGenerator = randomPlacemarkGenerator;
    }

    @Override
    public void run() {
        Placemark placemark = randomPlacemarkGenerator.generate();
        @SuppressWarnings("unchecked") AreaOfInterest aoi = (AreaOfInterest)remote.getAttribute(DataGenService.MARQUEE_ATTRIBUTE);
        if(PlacemarkFilter.placemarkIsWithinAreaOfInterest(aoi, placemark)) {
        	logger.debug("!!!!!!!!!!!! Placemark within AOI!!!!!!!!!!!!!");
	        Kml kml = KmlFactory.createKml();
	        kml.setFeature(placemark);
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        try {
	            kml.marshal(baos);
	            remote.deliver(serverSession, "/datagen/placemark", baos.toString(), null);
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	            logger.error(e.getMessage());
	        }
        }
        else {
        	logger.debug("Placemark outside AOI");
        }
    }
}
