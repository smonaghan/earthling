package earthling.cometd.service.datagen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.Vector;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.cometd.annotation.Listener;
import org.cometd.annotation.Service;
import org.cometd.annotation.Session;
import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.bayeux.server.ServerMessage;
import org.cometd.bayeux.server.ServerSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Geometry;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Point;

import earthling.cometd.domain.AreaOfInterest;
import earthling.cometd.service.datagen.task.RandomPlacemarkGeneratorTask;
import earthling.datagen.RandomPlacemarkGenerator;

/**
 * CometD service for the control and publishing of mock data
 */
@Named
@Singleton
@Service("dataGenService")
public class DataGenService {

    protected final static Logger logger = LoggerFactory.getLogger(DataGenService.class);
    public  final static String MARQUEE_ATTRIBUTE   = "marquee";
    private final static String LATITUDE_ATTRIBUTE  = "latitude";
    private final static String ALTITUDE_ATTRIBUTE  = "altitude";
    private final static String LONGITUDE_ATTRIBUTE = "longitude";
    

    @Autowired
    private RandomPlacemarkGenerator randomPlacemarkGenerator;

    @Inject
    private BayeuxServer bayeux;

    @Session
    private ServerSession serverSession;

    private Timer timer = new Timer();

    private RandomPlacemarkGeneratorTask randomPlacemarkGeneratorTask;

    @PostConstruct
    public void init() {
        logger.trace("init");
    }

    @Listener("/service/datagen/start")
    public void start(ServerSession remote, ServerMessage.Mutable message) {
        randomPlacemarkGeneratorTask = new RandomPlacemarkGeneratorTask(serverSession, remote, new RandomPlacemarkGenerator());
        timer.scheduleAtFixedRate(randomPlacemarkGeneratorTask, 0, 10000);
    }

    @Listener("/service/datagen/stop")
    public void stop(ServerSession remote, ServerMessage.Mutable message) {
        timer.cancel();
    }

    @Listener("/service/datagen/aoi")
    public void setAreaOfInterest(ServerSession remote, ServerMessage.Mutable message) throws JSONException {
    	remote.removeAttribute(MARQUEE_ATTRIBUTE);
    	Object messageObj = message.get("data");
    	AreaOfInterest aoi = convertAreaOfInterestJsonToObject(new JSONArray(messageObj));
    	remote.setAttribute(MARQUEE_ATTRIBUTE, aoi);
    	
    }
    
    private AreaOfInterest convertAreaOfInterestJsonToObject(JSONArray aoiJsonArray) throws JSONException {
    	int length = aoiJsonArray.length();
    	List<Coordinate> coordinates = new Vector<Coordinate>();
    	for(int index=0; index < length; index++) {
    		@SuppressWarnings("unchecked") Map<String, Double> map = (Map<String, Double>)aoiJsonArray.get(index);
    		Double altitude = map.get(ALTITUDE_ATTRIBUTE);
    		Double longitude = map.get(LONGITUDE_ATTRIBUTE);
    		Double latitude = map.get(LATITUDE_ATTRIBUTE);
    		Coordinate coordinate = new Coordinate(longitude, latitude, altitude);
    		coordinates.add(coordinate);
    	}
    	return new AreaOfInterest(coordinates);
    }

}
