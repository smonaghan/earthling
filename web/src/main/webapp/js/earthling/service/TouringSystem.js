define("earthling/service/TouringSystem", ['signals/Signals'],

    function(Signal) {

        // https://developers.google.com/earth/documentation/reference/interface_g_e_tour_player
        function TouringSystem(ge) {
            this.ge = ge;
            this.currentTour = null;
            this.$tourStarted = null;
            this.$tourPaused = null;
            this.$tourStopped = null;
            this.$tourChanged = null;
        }

        TouringSystem.prototype.setTour = function(tour){
            this.ge.getTourPlayer.setTour(tour);

            if(this.currentTour !== tour){
                this.$tourChanged.dispatch(tour);
            }
        };

        return TouringSystem;
    });
