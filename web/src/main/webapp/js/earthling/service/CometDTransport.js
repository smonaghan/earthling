define("earthling/service/CometDTransport", ['signals/Signals'],

    function(Signal) {

        var initialized = false;
        var subscriptions = {};

        function Transport() {
            this.$messageRecieved = new Signal();
            this.$initialized = new Signal();
            this.$faulted = new Signal();
            this.$disconnected = new Signal();
            this.lastMessageTime = null;
        }

        Transport.prototype.initialize = function(cometdUrl) {
            var self = this;
            if (!initialized) {
                $.cometd.addListener("/meta/handshake", function(message) {
                    if (message.successful) {
                        initialized = true;
                        self.$initialized.dispatch(message);
                        self.$initialized.removeAll();
                    } else {
                        self.$faulted.dispatch(message);
                    }
                });

                $.cometd.configure({
                    url: cometdUrl,
                    logLevel: 'info'
                });

                $.cometd.handshake();
            }
        }

        Transport.prototype.disconnect = function(channel) {
            this.$messageRecieved.removeAll();
            $.each(subscriptions, function(i, subscription) {
                $.cometd.unsubscribe(subscription, function(message) {
                });
            })

            subscriptions = {};
        }

        Transport.prototype.subscribe = function(channel) {
            if (subscriptions[channel] == undefined) {
                var self = this;
                subscriptions[channel] = $.cometd.subscribe(channel, function(message) {
                    self.lastMessageTime = new Date().getTime();
                    self.$messageRecieved.dispatch(message);
                });
            }
        }

        Transport.prototype.unsubscribe = function(channel) {
            var subscription = subscriptions[channel];
            if (subscription) {
                $.cometd.unsubscribe(subscription, function(message) {
                    delete subscriptions[channel];
                });
            } else {
                this.$faulted.dispatch('unable to subscribe from channel ' + channel + '. I\'m not currently subscribed to that channel.');
            }
        }

        Transport.prototype.publish = function(channel, message) {
            console.log('publishing on ' + channel);
            $.cometd.publish(channel, message);
        }

        Transport.prototype.getLastMessageTime = function() {
            return this.lastMessageTime;
        }

        return Transport;
    });