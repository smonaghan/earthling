// https://developers.google.com/kml/documentation/kmlreference#lookat
define("earthling/data/Locations", [],
    function() {
        return {
            Baltimore:{lat:39.283333, lon:-76.616667, range:5000},
            NewYork:{lat:40.664167, lon:-73.938611, range:230000}
        };
    }
);
