define("earthling/ui/MarqueeShape", ['earthling/ui/kml/KmlFactory', 'earthling/data/Colors', 'earthling/util', 'signals/Signals'],

    function(KmlFactory, Colors, util, Signal) {

        function MarqueeShape(ge, initLoc) {
            this.ge = ge;
            this.placemark = null;
            this.polygon = null;
            this.polygonOuter = null;
            this.pointLocations = [];
            this.$activated = new Signal();
            this.$deactivated = new Signal();

            // init main placemark
            this.placemark = this.ge.createPlacemark('');
            this.placemark.setName('');
            this.ge.getFeatures().appendChild(this.placemark);

            // main polygon
            this.polygon = ge.createPolygon(util.nextPolyId());
            this.polygon.setAltitudeMode(this.ge.ALTITUDE_CLAMP_TO_GROUND);
            this.polygon.setTessellate(1);
            this.placemark.setGeometry(this.polygon);

            // outer polygon
            this.polygonOuter = this.ge.createLinearRing('');
            this.polygonOuter.getCoordinates().pushLatLngAlt(initLoc.lat, initLoc.lon, initLoc.alt);
            this.polygon.setOuterBoundary(this.polygonOuter);

            // set styles
            this.placemark.setStyleSelector(this.ge.createStyle(''));
            var lineStyle = this.placemark.getStyleSelector().getLineStyle();
            lineStyle.setWidth(2);
            lineStyle.getColor().set(Colors.Marquee.stroke);

            var polyStyle = this.placemark.getStyleSelector().getPolyStyle();
            polyStyle.getColor().set(Colors.Marquee.fill);

            // add feature
            this.ge.getFeatures().appendChild(this.placemark);

            var self = this;
            google.earth.addEventListener(this.ge.getWindow(), 'click', function(event) {
                var geom = event.getTarget().getGeometry();
                if(geom && geom.getId() == self.polygon.getId()){
                    self.activate();
                }
            });

            // place marker
            KmlFactory.newPinkDot(this.ge, initLoc);
        }

        MarqueeShape.prototype.expand = function(loc) {
            // place marker
            KmlFactory.newPinkDot(this.ge, loc);

            // expand polygon
            this.polygonOuter.getCoordinates().pushLatLngAlt(loc.lat, loc.lon, loc.alt);
        }

        MarqueeShape.prototype.activate = function() {
            var lineStyle = this.placemark.getStyleSelector().getLineStyle();
            lineStyle.setWidth(2);
            lineStyle.getColor().set(Colors.Marquee.stroke);

            var polyStyle = this.placemark.getStyleSelector().getPolyStyle();
            polyStyle.getColor().set(Colors.Marquee.fill);

            this.$activated.dispatch(this);
        }

        MarqueeShape.prototype.deactivate = function() {
            console.log('should deactivate');
            var lineStyle = this.placemark.getStyleSelector().getLineStyle();
            lineStyle.setWidth(2);
            lineStyle.getColor().set(Colors.MarqueeInactive.stroke);

            var polyStyle = this.placemark.getStyleSelector().getPolyStyle();
            polyStyle.getColor().set(Colors.MarqueeInactive.fill);

            this.$deactivated.dispatch(this);
        }

        MarqueeShape.prototype.destroy = function() {
            this.ge.getFeatures().removeChild(this.placemark);
            google.earth.addEventListener(this.polygon, 'click', this._onclick);
        }

        // delegate
        MarqueeShape.prototype._onclick = function(kmlEvent) {
            console.log('handling onclick');
            kmlEvent.stopPropagation();
            this.activate();
        }

        return MarqueeShape;
    });