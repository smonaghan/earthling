define("earthling/ui/kml/KmlFactory", [],
    function() {
        return {
            newPinkDot:function(ge, config) {
                // Create the placemark.
                var placemark = ge.createPlacemark('');
                placemark.setName('');

                // Define a custom icon.
                var icon = ge.createIcon('');
                var url = (window.location.href+'/img/bullet_pink.png').replace('#', '');
                icon.setHref(url);
                var style = ge.createStyle(''); //create a new style
                style.getIconStyle().setIcon(icon); //apply the icon to the style
                style.getIconStyle().setScale(0.5);
                placemark.setStyleSelector(style); //apply the style to the placemark

                // Set the placemark's location.
                var point = ge.createPoint('');
                point.setLatitude(config.lat);
                point.setLongitude(config.lon);
                point.setAltitude(config.alt);
                placemark.setGeometry(point);

                // Add the placemark to Earth.
                ge.getFeatures().appendChild(placemark);
                return placemark;
            }
        };
    }
);
