define("earthling/ui/MarqueeTool",
    ['earthling/ui/MarqueeShape', 'earthling/ui/kml/KmlFactory', 'earthling/data/Colors', 'earthling/util', 'signals/Signals'],
    function(MarqueeShape, KmlFactory, Colors, util, Signal) {

        function MarqueeTool(ge, activateElementSelector) {
            this.ge = ge;
            this.marquees = [];
            this.drawing = false;
            this.$opened = new Signal();
            this.$closed = new Signal();
            this.$dragging = new Signal();
            this.$aoi = new Signal();

            // init delegate
            var self = this;
            var delegate = {
                onClick:function(kmlEvent) {
                    if (self.drawing) {
                        kmlEvent.preventDefault();
                        var loc = util.kmlEventToLoc(kmlEvent);
                        if (!self.activeMarquee) {
                            var am = new MarqueeShape(self.ge, loc);
                            am.$activated.add(function(activatedMarquee) {
                                console.log('activated!!');
                                if (self.activeMarquee) {
                                    self.activeMarquee.deactivate();
                                }
                                self.activeMarquee = activatedMarquee;
                            });

                            self.activeMarquee = am;
                            console.log('self.activeMarquee: ' + self.activeMarquee);
                            self.marquees.push(am);
                        } else {
                            self.activeMarquee.expand(loc);
                        }
                    } else {
                        // do nothing
                    }
                },

                onDblClick:function(kmlEvent) {
                    kmlEvent.preventDefault();
                    if (self.activeMarquee) {
                        disableDrawingMode();
                    }
                }
            };

            // init lasso button
            this.activateElement = $(activateElementSelector);
            this.activateElement.click(function(event) {
                if (!self.drawing) {
                    enableDrawingMode();
                } else {
                    disableDrawingMode();
                }
            });

            function enableDrawingMode() {
                self.drawing = true;
                google.earth.addEventListener(ge.getGlobe(), 'click', delegate.onClick);
                google.earth.addEventListener(ge.getGlobe(), 'dblclick', delegate.onDblClick);
                $('body').css('cursor', 'crosshair');
            }

            function disableDrawingMode() {
                self.drawing = false;
                google.earth.removeEventListener(ge.getGlobe(), 'click', delegate.onClick);
                google.earth.removeEventListener(ge.getGlobe(), 'dblclick', delegate.onDblClick);
                $('body').css('cursor', 'default');

                if (self.activeMarquee) {
                    self.activeMarquee.deactivate();
                    self.activeMarquee = null;
                }
            }

        }

        MarqueeTool.prototype.clearAll = function() {
            this.ge.getFeatures().removeChild(this.marqueePlacemark);
        };

        return MarqueeTool;

    }
);
