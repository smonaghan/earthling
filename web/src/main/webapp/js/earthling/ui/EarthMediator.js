define("earthling/ui/EarthMediator", ['earthling/ui/MarqueeTool','signals/Signals'],

    function(MarqueeTool, Signal) {

        function EarthMediator(elementId) {
            this.elementId = elementId;
            this.ge = null;
            this.marqueeTool = null;
            this.$loaded = new Signal();
            this.$error = new Signal();
            this.$aoiUpdate = new Signal();
        }

        EarthMediator.prototype.initialize = function() {
            console.info("loading google earth api");
            var self = this;
            google.load("earth", "1", {"callback" : function() {
                console.info("initializing google earth plugin");
                google.earth.createInstance(self.elementId, function(instance) {
                        self.ge = instance;
                        self.ge.getWindow().setVisibility(true);
                        self.ge.getNavigationControl().setVisibility(self.ge.VISIBILITY_AUTO);
                        self.$loaded.dispatch("google earth successfully loaded");
                    },
                    function(errorCode) {
                        self.$error.dispatch("google earth failed to load. error code: " + errorCode);
                    });
            }
            });
        };

        EarthMediator.prototype.initializeMarqueeTool = function() {
            this.marqueeTool = new MarqueeTool(this.ge, '#lassoBtn');
            var self = this;
            this.marqueeTool.$aoi.add(function(aoi) {
                self.$aoiUpdate.dispatch(aoi);
            })
        };

        EarthMediator.prototype.getGe = function() {
            return this.ge;
        };

        EarthMediator.prototype.appendKmlString = function(kmlString) {
            this.appendKml(this.ge.parseKml(kmlString));
        };

        EarthMediator.prototype.appendKml = function(kml) {
            this.ge.getFeatures().appendChild(kml);
        };

        EarthMediator.prototype.getVersion = function() {
            return {'apiVersion':this.ge.getApiVersion(),
                'pluginVersion':this.ge.getPluginVersion()};
        };

        EarthMediator.prototype.lookAt = function(loc) {
            var lookAt = this.ge.createLookAt('');
            lookAt.setLatitude(loc.lat);
            lookAt.setLongitude(loc.lon);
            lookAt.setRange(loc.range);
            this.ge.getView().setAbstractView(lookAt);
        };

        return EarthMediator;
    });
