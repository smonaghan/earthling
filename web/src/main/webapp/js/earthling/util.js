define("earthling/util", [],
    function() {

        var polyId = 0;
        return {
            logKmlEvent:function(kmlEvent, msg) {
                console.log(msg + ': ' + this.kmlEventToLoc(kmlEvent));
            },

            kmlEventToLoc:function(kmlEvent) {
                return {lat:kmlEvent.getLatitude(), lon:kmlEvent.getLongitude(), alt:kmlEvent.getAltitude()};
            },

            nextPolyId:function() {
                return 'polygon-'+(polyId++);
            }
        };
    }
);
