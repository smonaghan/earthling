define(['earthling/ui/EarthMediator','earthling/data/Locations','earthling/service/CometDTransport','signals/Signals', 'ich/ICanHaz'],
    function(EarthMediator, Locations, CometDTransport, Signal, ich) {
        return {
            $started:new Signal(),
            $faulted:new Signal(),

            run:function() {
                var self = this;

                // initialize cometd
                this.cometDTransport = new CometDTransport();

                this.cometDTransport.$initialized.add(function(msg) {
                    self.cometDTransport.subscribe('/datagen/placemark');

                    // not starting datagen automatically
                    self.cometDTransport.publish('/service/datagen/start', {});
                });

                this.cometDTransport.$faulted.add(function(msg) {
                    self.$faulted.dispatch(msg);
                });

                this.cometDTransport.$messageRecieved.add(function(message) {
                    console.log(message.channel);
                    self.earthMediator.appendKmlString(message.data);
                });

                this.cometDTransport.initialize(location.protocol + "//" + location.host + "/cometd");

                // initialize mediator
                this.earthMediator = new EarthMediator('earth');
                this.earthMediator.$loaded.add(function(msg) {
                    self.$started.dispatch("earthling successfully started");

                    // move this to more appropriate layer - 'GUIInitializer' or some such
                    $('#header').append(ich.headerAndVersionLabel(self.earthMediator.getVersion()));
                    self.earthMediator.initializeMarqueeTool();
                    self.earthMediator.$aoiUpdate.add(function(aoi) {
                        self.cometDTransport.publish('/service/datagen/aoi', aoi);
                    });

                    //console.debug(self.earthMediator.getVersion());
                    self.earthMediator.lookAt(Locations.NewYork);
                });

                this.earthMediator.$error.add(function(msg) {
                    self.$faulted.dispatch("earthling failed to start");
                });

                this.earthMediator.initialize();
            }


        };
    }
);
